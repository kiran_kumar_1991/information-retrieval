package com.kiran.info.retrieval.services;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kiran.info.retrieval.api.ILemmatizer;
import com.kiran.info.retrieval.util.DirectoryReader;
import com.kiran.info.retrieval.util.FileReader;
import com.kiran.info.retrieval.util.StringUtil;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class LemmatizerService implements ILemmatizer {

	private static final Logger logger = LogManager.getLogger(LemmatizerService.class);

	protected StanfordCoreNLP pipeline;

	private static final String ALPHA_NUMERIC_REGEX = "\\b(\\w+)\\b";
	private static final String NUMERIC_REGEX = "\\b(\\d+)\\b";
	private Pattern alphaNumPattern = Pattern.compile(ALPHA_NUMERIC_REGEX);
	private Pattern numPattern = Pattern.compile(NUMERIC_REGEX);

	public LemmatizerService() {
		// Create StanfordCoreNLP object properties, with POS tagging
		// (required for lemmatization), and lemmatization
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");

		// StanfordCoreNLP loads a lot of models, so you probably
		// only want to do this once per execution
		this.pipeline = new StanfordCoreNLP(props);
	}

	/**
	 * Lemmatizes the given text
	 * 
	 * @param documentText
	 * @return List of lemmas
	 */
	@Override
	public List<String> lemmatize(String documentText) {
		List<String> lemmas = new LinkedList<String>();

		// create an empty Annotation just with the given text
		Annotation document = new Annotation(documentText);

		// run all Annotators on this text
		this.pipeline.annotate(document);

		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			// Iterate over all tokens in a sentence
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				// Retrieve and add the lemma for each word into the list of
				// lemmas
				lemmas.add(token.get(LemmaAnnotation.class));
			}
		}

		return lemmas;
	}

	/**
	 * Lemmatizes the given document
	 * 
	 * @param directory
	 * @return Map of tokens versus their frequency
	 */
	@Override
	public Map<String, Integer> lemmatizeDocument(File filePath) {
		Map<String, Integer> auxillaryMap = new HashMap<String, Integer>();
		try {
			List<String> lines = FileReader.readAllLines(filePath.getAbsolutePath());
			StringBuilder builder = new StringBuilder();
			lines.forEach(line -> {
				String content = StringUtil.removeTags(line);
				builder.append(content).append(" ");
			});
			List<String> lemmas = lemmatize(builder.toString());
			lemmas.forEach(lemma -> {
				Matcher alphaNumMatcher = alphaNumPattern.matcher(lemma);
				Matcher numMatcher = numPattern.matcher(lemma);
				if (alphaNumMatcher.matches()) {
					auxillaryMap.putIfAbsent(lemma, 0);
					auxillaryMap.put(lemma, auxillaryMap.get(lemma) + 1);
				}
			});
		} catch (IOException e) {
			logger.error("Exception encountered when lemmatizing the file :{}", filePath.getAbsolutePath(), e);
		} catch (Exception e) {
			logger.error("Exception encountered when lemmatizing the file: {}", filePath.getAbsolutePath(), e);
		}
		return auxillaryMap;
	}

	/**
	 * Lemmatizes all the documents in the given directory
	 * 
	 * @param directory
	 * @return Map of tokens versus their frequency
	 */
	@Override
	public Map<String, Integer> lemmatizeDocumentsInDirectory(String directoryPath) {
		Map<String, Integer> consolidatedMap = new TreeMap<String, Integer>();
		List<File> files = DirectoryReader.getFilesInDirectory(directoryPath);
		files.forEach(file -> {
			Map<String, Integer> auxillaryMap = lemmatizeDocument(file);
			mergeMaps(consolidatedMap, auxillaryMap);
		});
		return consolidatedMap;
	}

	/**
	 * Merges the contents of the auxillaryMap with the consolidated map by
	 * adding the frequencies
	 * 
	 * @param consolidatedMap
	 * @param auxillaryMap
	 * @return ConsolidatedMap with tokens along with their combined frequencies
	 */
	private Map<String, Integer> mergeMaps(Map<String, Integer> consolidatedMap, Map<String, Integer> auxillaryMap) {
		auxillaryMap.forEach((key, value) -> consolidatedMap.merge(key, value, Math::addExact));
		return consolidatedMap;
	}
}