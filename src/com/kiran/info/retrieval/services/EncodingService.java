package com.kiran.info.retrieval.services;

import java.util.Arrays;
import java.util.BitSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kiran.info.retrieval.api.IEncoder;
import com.kiran.info.retrieval.model.EncodingFormat;
import com.kiran.info.retrieval.util.BinaryConversionUtil;

public class EncodingService implements IEncoder {

	private static final Logger logger = LogManager.getLogger(EncodingService.class);

	@Override
	public BitSet encodeValue(int value, EncodingFormat encodingFormat) {
		BitSet encodedValue = null;
		if (encodingFormat.equals(EncodingFormat.UNARY))
			encodedValue = unaryEncode(value);
		else if (encodingFormat.equals(EncodingFormat.GAMMA))
			encodedValue = gammaEncode(value);
		else if (encodingFormat.equals(EncodingFormat.DELTA))
			encodedValue = deltaEncode(value);
		else
			logger.error("Unsupported encoding format:{}", encodingFormat.toString());
		return encodedValue;
	}

	public BitSet unaryEncode(int number) {
		boolean[] result = new boolean[number + 1];
		for (int i = 0; i < number; i++)
			result[i] = true;
		result[number] = false;
		return BinaryConversionUtil.convertBooleanToBitSet(result);
	}

	public BitSet gammaEncode(int number) {
		String binaryString = Integer.toBinaryString(number);
		boolean[] bits = BinaryConversionUtil.convertBinaryStringToBooleanArray(binaryString);
		boolean[] offset = Arrays.copyOfRange(bits, 1, bits.length);
		boolean[] unaryCode = BinaryConversionUtil.convertBitSetToBoolean(unaryEncode(offset.length));
		boolean[] result = new boolean[unaryCode.length + offset.length];
		System.arraycopy(unaryCode, 0, result, 0, unaryCode.length);
		System.arraycopy(offset, 0, result, unaryCode.length, offset.length);
		return BinaryConversionUtil.convertBooleanToBitSet(result);
	}

	public BitSet deltaEncode(int number) {
		String binaryString = Integer.toBinaryString(number);
		boolean[] bits = BinaryConversionUtil.convertBinaryStringToBooleanArray(binaryString);
		boolean[] offset = Arrays.copyOfRange(bits, 1, bits.length);
		boolean[] gammaCode = BinaryConversionUtil.convertBitSetToBoolean(gammaEncode(bits.length));
		boolean[] result = new boolean[gammaCode.length + offset.length];
		System.arraycopy(gammaCode, 0, result, 0, gammaCode.length);
		System.arraycopy(offset, 0, result, gammaCode.length, offset.length);
		return BinaryConversionUtil.convertBooleanToBitSet(result);
	}

	public static void main(String[] args) {
		EncodingService encoder = new EncodingService();
		for (int number = 0; number <= 10; number++) {
			System.out.println("\n\n Number:" + number);
			encoder.testUnaryEncoding(encoder, number);
			encoder.testGammaEncoding(encoder, number);
			encoder.testDeltaEncoding(encoder, number);
		}
	}

	private void testDeltaEncoding(EncodingService encoder, int number) {
		System.out.println("Delta: " + BinaryConversionUtil.convertBitSetToBinaryString(encoder.deltaEncode(number)));
	}

	private void testGammaEncoding(EncodingService encoder, int number) {
		BitSet gammaEncode = encoder.gammaEncode(number);
		System.out.println("Gamma: " + BinaryConversionUtil.convertBitSetToBinaryString(gammaEncode));
	}

	private void testUnaryEncoding(EncodingService encoder, int number) {
		System.out.println("Unary: " + BinaryConversionUtil.convertBitSetToBinaryString(encoder.unaryEncode(number)));
	}

}
