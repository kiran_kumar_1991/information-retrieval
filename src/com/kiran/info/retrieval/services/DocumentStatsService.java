package com.kiran.info.retrieval.services;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kiran.info.retrieval.api.IDocumentStats;
import com.kiran.info.retrieval.api.IEncoder;
import com.kiran.info.retrieval.model.EncodingFormat;

public class DocumentStatsService implements IDocumentStats {

	private static final Logger logger = LogManager.getLogger(DocumentStatsService.class);

	private Map<Integer, Map<String, Object>> documentStats = new HashMap<Integer, Map<String, Object>>();
	private IEncoder encoder;

	public DocumentStatsService(IEncoder encoder) {
		this.encoder = encoder;
	}

	/**
	 * Stores the statistics of the given documentId. It is merged with the
	 * existing statistics for the document, if present, and overwrites the same
	 * entries.
	 * 
	 * @param docId
	 * @param stats
	 */
	@Override
	public void addDocumentStats(Integer docId, Map<String, Object> stats) {
		documentStats.putIfAbsent(docId, new HashMap<String, Object>());
		Map<String, Object> map = documentStats.get(docId);
		map.putAll(stats);
	}

	/**
	 * Fetches the statistics for the given document Id.
	 * 
	 * @param docId
	 * @return Map<String, Object> statistics
	 */
	@Override
	public Map<String, Object> getStatsForDocumentId(Integer docId) {
		return documentStats.get(docId);
	}

	/**
	 * Print the statistics for all the documents
	 */
	@Override
	public void printDocumentsStats() {
		documentStats.forEach((docId, attributeMap) -> {
			logger.info("DocId: {}. Attribute Map:{}", docId, attributeMap);
		});
	}

	/**
	 * Returns the statistics of all documents in a Map. Map of documentId
	 * versus Map of Document attribute name against values
	 * 
	 * @return Map<Integer, Map<String, Object>> statistics
	 */

	@Override
	public Map<Integer, Map<String, Object>> getAllDocumentStats() {
		return documentStats;
	}

	/**
	 * Returns the statistics of all documents in a Map. Map of documentId
	 * versus Map of Document attribute name against values. The attribute
	 * values are compressed based on the encoding format provided in the
	 * argument
	 * 
	 * @return Map<Integer, Map<String, byte[]>> statistics
	 */
	@Override
	public Map<Integer, Map<String, byte[]>> getCompressedStats(EncodingFormat encodingFormat) {
		Map<Integer, Map<String, byte[]>> compressedStats = new HashMap<Integer, Map<String, byte[]>>();
		documentStats.forEach((docId, docStats) -> {
			Map<String, byte[]> compressedAttributes = new HashMap<String, byte[]>();
			docStats.forEach((attribute, value) -> {
				if (value instanceof Integer) {
					BitSet encodedValue = null;
					encodedValue = encoder.encodeValue((Integer) value, encodingFormat);
					compressedAttributes.put(attribute, encodedValue.toByteArray());
				} else {
					try {
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						ObjectOutputStream oos = new ObjectOutputStream(bos);
						oos.writeObject(value);
						compressedAttributes.put(attribute, bos.toByteArray());
					} catch (Exception e) {
						logger.error("Exception during serialization of object for attribute:{}", attribute);
					}
				}
			});
		});
		return compressedStats;
	}

}
