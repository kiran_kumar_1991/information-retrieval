package com.kiran.info.retrieval.services;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kiran.info.retrieval.api.ITokenizer;
import com.kiran.info.retrieval.util.DirectoryReader;
import com.kiran.info.retrieval.util.FileReader;
import com.kiran.info.retrieval.util.StringUtil;

public class TokenizerService implements ITokenizer {

	private static final Logger logger = LogManager.getLogger(TokenizerService.class);
	private static final String EXTRACT_WORD_REGEX = "\\b(\\w+)\\b";

	private Pattern pattern = Pattern.compile(EXTRACT_WORD_REGEX, Pattern.CASE_INSENSITIVE);

	/**
	 * Tokenizes the document given as argument
	 * 
	 * @param filePath
	 * @return Map of tokens versus their frequency
	 */
	@Override
	public Map<String, Integer> tokenizeDocument(File filePath) {
		Map<String, Integer> auxillaryMap = new HashMap<String, Integer>();
		try {
			List<String> lines = FileReader.readAllLines(filePath.getAbsolutePath());
			lines.forEach(line -> {
				String content = StringUtil.removeTags(line);
				extractTokens(content.toLowerCase(), auxillaryMap);
			});
		} catch (IOException e) {
			logger.error("Exception encountered while reading the file", e);
		} catch (Exception e) {
			logger.error("Exception encountered when proceesing the file: {}", filePath.getAbsolutePath(), e);
		}
		return auxillaryMap;
	}

	/**
	 * Extracts tokens from the content passed as argument and adds to the
	 * auxillaryMap passed as argument to the method
	 * 
	 * @param content
	 * @param auxillaryMap
	 */
	private void extractTokens(String content, Map<String, Integer> auxillaryMap) {
		Matcher matcher = pattern.matcher(content);
		while (matcher.find()) {
			String token = matcher.group();
			auxillaryMap.putIfAbsent(token, 0);
			Integer count = auxillaryMap.get(token);
			auxillaryMap.put(token, count + 1);
		}
	}

	/**
	 * Tokenizes all the documents in the given directory
	 * 
	 * @param directory
	 * @return Map of tokens versus their frequency
	 */

	@Override
	public Map<String, Integer> tokenizeDocumentsInDirectory(String directoryPath) {
		Map<String, Integer> consolidatedMap = new HashMap<String, Integer>();
		List<File> files = DirectoryReader.getFilesInDirectory(directoryPath);
		files.forEach(file -> {
			Map<String, Integer> auxillaryMap = tokenizeDocument(file);
			mergeMaps(consolidatedMap, auxillaryMap);
		});
		return consolidatedMap;
	}

	/**
	 * Merges the contents of the auxillaryMap with the consolidated map by
	 * adding the frequencies
	 * 
	 * @param consolidatedMap
	 * @param auxillaryMap
	 * @return ConsolidatedMap with tokens along with their combined frequencies
	 */

	private Map<String, Integer> mergeMaps(Map<String, Integer> consolidatedMap, Map<String, Integer> auxillaryMap) {
		auxillaryMap.forEach((key, value) -> consolidatedMap.merge(key, value, Math::addExact));
		return consolidatedMap;
	}

	private void testMergeMaps() {
		Map<String, Integer> m1 = new HashMap<String, Integer>();
		Map<String, Integer> m2 = new HashMap<String, Integer>();
		m1.put("a", 2);
		m1.put("b", 3);
		m2.put("a", 3);
		System.out.println(mergeMaps(m1, m2));
	}

	private void regexTest() {
		String line = "<DOC attr=val>abc<vbb></doc> ";
		System.out.println(line.replaceAll("(<([^>])+>)", ""));
		String data1 = "Today's,.;eqer;java is object-oriented language";
		String regex = "\\b(\\w+)\\b";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(data1);
		while (matcher.find()) {
			System.out.println(matcher.group());
		}
	}

	public static void main(String[] args) {
		TokenizerService service = new TokenizerService();
		service.regexTest();
		Map<String, Integer> tokens = service.tokenizeDocumentsInDirectory(
				"/Users/KiranKumar/Documents/OneDrive/Kiran Mac/Masters/Studies/Information Retrieval/Homeworks/Homework 1/Cranfield");
		System.out.println(tokens);

	}

}
