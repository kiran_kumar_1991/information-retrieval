package com.kiran.info.retrieval.services;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kiran.info.retrieval.api.IStopWordDetector;
import com.kiran.info.retrieval.util.FileReader;
import com.kiran.info.retrieval.util.StringUtil;

public class StopWordService implements IStopWordDetector {

	private static final Logger logger = LogManager.getLogger(StopWordService.class);

	private HashSet<String> stopWords = new HashSet<String>();

	public StopWordService(String absolutePath) {
		try {
			logger.debug("Reading the stop words from the file: {}", absolutePath);
			List<String> lines = FileReader.readAllLines(absolutePath);
			stopWords.addAll(lines);
			logger.debug("Finished reading stop words from the file: {}", absolutePath);
		} catch (IOException e) {
			logger.error("Exception encountered while reading stop words from the file: {}", absolutePath, e);
		}
	}

	public boolean isStopWord(String word) {
		if (StringUtil.isEmpty(word))
			return false;
		return stopWords.contains(word);
	}

}
