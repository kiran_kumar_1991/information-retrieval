package com.kiran.info.retrieval.services;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kiran.info.retrieval.api.IDocumentStats;
import com.kiran.info.retrieval.api.IRankedRetrievalService;
import com.kiran.info.retrieval.model.DocumentResult;
import com.kiran.info.retrieval.model.DocumentStatsAttribute;
import com.kiran.info.retrieval.model.Posting;
import com.kiran.info.retrieval.model.Query;
import com.kiran.info.retrieval.model.RankedResult;
import com.kiran.info.retrieval.model.Vector;
import com.kiran.info.retrieval.model.WeightingFunction;
import com.kiran.info.retrieval.util.WeightingFunctionUtil;

public class RankedRetrievalService implements IRankedRetrievalService {

	private static final Logger logger = LogManager.getLogger(RankedRetrievalService.class);

	private Map<String, List<Posting>> index;
	private IDocumentStats documentStats;

	public RankedRetrievalService(Map<String, List<Posting>> index, IDocumentStats documentStats) {
		this.index = index;
		this.documentStats = documentStats;
	}

	/**
	 * Queries the constructed index using the given weighting function
	 * 
	 * @param query
	 * @param weightingFunction
	 * @param limit
	 * @return RankedResult object
	 */
	@Override
	public RankedResult fetchResults(Query query, WeightingFunction weightingFunction, int limit) {

		// Count the frequency of the words in the query
		Map<String, Integer> countMap = query.getWordVsFrequency();
		Map<String, Vector> termVsQueryVectors = computeQueryVectors(query, weightingFunction, countMap);
		List<DocumentResult> documentResults = computeDocumentVectors(query, weightingFunction, countMap.keySet());

		// Normalize query vectors
		WeightingFunctionUtil.computeAndSetNormalizedValues(termVsQueryVectors.values());

		// Normalize document vectors
		documentResults.forEach(docResult -> {
			Map<String, Vector> termVsVector = docResult.getTermVsVector();
			WeightingFunctionUtil.computeAndSetNormalizedValues(termVsVector.values());
		});

		documentResults.forEach(docResult -> {
			Map<String, Vector> termVsVector = docResult.getTermVsVector();
			for (Entry<String, Vector> entry : termVsVector.entrySet()) {
				String term = entry.getKey();
				Vector queryTermVector = termVsQueryVectors.get(term);
				Vector docTermVector = entry.getValue();
				docResult.setScore(queryTermVector.getNormalizedValue() * docTermVector.getNormalizedValue());
			}
		});

		// Sort the document result based on decreasing order of their scores
		documentResults.sort((doc1, doc2) -> {
			double diff = doc2.getScore() - doc1.getScore();
			if (diff < 0)
				return -1;
			else if (diff > 0)
				return 1;
			else
				return 0;
		});

		RankedResult result = new RankedResult(termVsQueryVectors.values(), documentResults.subList(0, limit));

		return result;
	}

	/**
	 * Computes Document vectors for the given query and weighting function
	 * 
	 * @param query
	 * @param weightingFunction
	 * @param words
	 * @return
	 */
	private List<DocumentResult> computeDocumentVectors(Query query, WeightingFunction weightingFunction,
			Set<String> words) {
		Map<Integer, DocumentResult> docIdVsDocResult = new HashMap<Integer, DocumentResult>();
		int collectionSize = documentStats.getAllDocumentStats().size();
		for (String word : words) {
			List<Posting> postingsList = index.get(word);
			if (postingsList == null) {
				logger.warn("Unable to find the word:{} in the index", word);
				continue;
			}
			for (Posting posting : postingsList) {
				Integer documentId = posting.getDocumentId();

				docIdVsDocResult.putIfAbsent(documentId, new DocumentResult(documentId,
						(String) documentStats.getStatsForDocumentId(documentId)
								.get(DocumentStatsAttribute.DOC_TITLE.toString()),
						(String) documentStats.getStatsForDocumentId(documentId)
								.get(DocumentStatsAttribute.DOC_PATH.toString()),
						new HashMap<String, Vector>()));

				DocumentResult documentResult = docIdVsDocResult.get(documentId);
				Map<String, Vector> termVsVector = documentResult.getTermVsVector();
				Vector docVector;
				Map<String, Object> docStats = documentStats.getStatsForDocumentId(documentId);

				if (docStats == null)
					logger.error("Unable to find stats for documentId:{}", documentId);

				if (weightingFunction.equals(WeightingFunction.WF1)) {
					Integer maxTf = (Integer) (docStats.get(DocumentStatsAttribute.MAX_TF.toString()));
					docVector = new Vector(word, posting.getTermFrequency(), maxTf, collectionSize, 1);
					termVsVector.put(word, docVector);
				} else if (weightingFunction.equals(WeightingFunction.WF2)) {
					Integer docLen = (Integer) (docStats.get(DocumentStatsAttribute.DOC_LENGTH.toString()));
					float avgDocLength = (float) (docLen / collectionSize);
					docVector = new Vector(word, posting.getTermFrequency(), collectionSize, 1, avgDocLength, docLen);
					termVsVector.put(word, docVector);
				} else {
					logger.error("Unsupported weighting function: {}", weightingFunction);
				}
			}
		}

		return new LinkedList<>(docIdVsDocResult.values());

	}

	/**
	 * Computes the query vectors based on the given query and weighting
	 * function
	 * 
	 * @param query
	 * @param weightingFunction
	 * @param countMap
	 * @return
	 */
	private Map<String, Vector> computeQueryVectors(Query query, WeightingFunction weightingFunction,
			Map<String, Integer> countMap) {
		Map<String, Vector> queryVectors = new HashMap<String, Vector>();

		// Find out MaxTF
		int maxTf = 0;
		for (Entry<String, Integer> entry : countMap.entrySet()) {
			if (entry.getValue() > maxTf)
				maxTf = entry.getValue();
		}

		int collectionSize = documentStats.getAllDocumentStats().size();

		// Generate the query vectors based on the weighting function
		for (Entry<String, Integer> entry : countMap.entrySet()) {
			int docFreq = 0;
			String term = entry.getKey();
			if (index.get(term) != null) {
				docFreq = index.get(term).size();
			} else {
				logger.warn(
						"Unable to find term:{} in the dictionary. Setting docFreq to collection size, so as to make the weighting function return 0",
						term);
				docFreq = collectionSize;
			}
			Vector queryVector;
			Integer termFreq = countMap.get(term);
			if (weightingFunction.equals(WeightingFunction.WF1)) {
				queryVector = new Vector(term, termFreq, maxTf, collectionSize, docFreq);
				queryVectors.put(term, queryVector);
			} else if (weightingFunction.equals(WeightingFunction.WF2)) {
				queryVector = new Vector(term, termFreq, collectionSize, docFreq, 1, 1);
				queryVectors.put(term, queryVector);
			} else {
				logger.error("Unsupported weighting function: {}", weightingFunction);
			}
		}
		return queryVectors;
	}

}
