package com.kiran.info.retrieval.services;

import java.io.File;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kiran.info.retrieval.api.IDocumentStats;
import com.kiran.info.retrieval.api.IEncoder;
import com.kiran.info.retrieval.api.IIndexer;
import com.kiran.info.retrieval.api.ILemmatizer;
import com.kiran.info.retrieval.api.IStopWordDetector;
import com.kiran.info.retrieval.api.ITokenizer;
import com.kiran.info.retrieval.model.CompressedBlock;
import com.kiran.info.retrieval.model.CompressedDictionary;
import com.kiran.info.retrieval.model.CompressedPosting;
import com.kiran.info.retrieval.model.DocumentStatsAttribute;
import com.kiran.info.retrieval.model.EncodingFormat;
import com.kiran.info.retrieval.model.Posting;
import com.kiran.info.retrieval.util.DirectoryReader;
import com.kiran.info.retrieval.util.DocumentIdGenerator;
import com.kiran.info.retrieval.util.FileReader;
import com.kiran.info.retrieval.util.Stemmer;
import com.kiran.info.retrieval.util.StringUtil;

public class IndexerService implements IIndexer {

	private static final Logger logger = LogManager.getLogger(IndexerService.class);

	private ITokenizer tokenizer;
	private IStopWordDetector stopWordDetector;
	private IDocumentStats docStatsService;
	private IEncoder encoder;
	private ILemmatizer lemmatizer;

	public IndexerService(ITokenizer tokenizer, ILemmatizer lemmatizer, IStopWordDetector stopWordDetector,
			IDocumentStats docStatsService, IEncoder encoder) {
		this.tokenizer = tokenizer;
		this.lemmatizer = lemmatizer;
		this.stopWordDetector = stopWordDetector;
		this.docStatsService = docStatsService;
		this.encoder = encoder;
	}

	/**
	 * Generates an index of all the documents contained in the specified
	 * directory path. Based on the arguments passed, it does stemming and
	 * lemmatization of the tokens, if needed.
	 * 
	 * @param directoryPath
	 * @param doStemming
	 * @param doLemmatization
	 * @return Map<String, List<Posting>> - Tokens against the Posting list
	 */
	@Override
	public Map<String, List<Posting>> generateIndex(String directoryPath, boolean doStemming, boolean doLemmatization) {
		// Using a TreeMap instead of a HashMap as the index should be sorted
		// based on the terms.
		Map<String, List<Posting>> index = new TreeMap<String, List<Posting>>();
		DocumentIdGenerator.resetCounter();
		if (StringUtil.isEmpty(directoryPath)) {
			logger.error("The directory path is either null/empty");
			return index;
		}
		List<File> files = DirectoryReader.getFilesInDirectory(directoryPath);
		for (File file : files) {
			Map<String, Integer> tokens = null;
			// Lemmatize the documents, if asked to do.
			if (doLemmatization)
				tokens = lemmatizer.lemmatizeDocument(file);
			else
				tokens = tokenizer.tokenizeDocument(file);
			Map<String, Object> docStats = new HashMap<String, Object>();
			docStats.put(DocumentStatsAttribute.DOC_TITLE.toString(),
					FileReader.getDocumentTitle(file.getAbsolutePath()));
			docStats.put(DocumentStatsAttribute.DOC_PATH.toString(), file.getAbsolutePath());
			addTokensToIndex(index, tokens, doStemming, doLemmatization, docStats);
		}
		return index;
	}

	private void addTokensToIndex(Map<String, List<Posting>> index, Map<String, Integer> tokens, boolean doStemming,
			boolean doLemmatization, Map<String, Object> docStats) {
		int documentId = DocumentIdGenerator.getDocumentId();
		int freqOfMostFreqWord = 0;
		int noOfWords = 0;
		int docLength = 0;

		// Stem the tokens, if asked to do.
		if (doStemming)
			tokens = Stemmer.stemTokens(tokens);

		for (Entry<String, Integer> entry : tokens.entrySet()) {
			String token = entry.getKey();
			Integer freq = entry.getValue();
			noOfWords += freq.intValue();
			if (!stopWordDetector.isStopWord(token)) {
				docLength += freq;
				if (freq.intValue() > freqOfMostFreqWord) {
					freqOfMostFreqWord = freq;
				}
				index.putIfAbsent(token, new ArrayList<Posting>());
				List<Posting> posting = index.get(token);
				posting.add(new Posting(documentId, freq));
			}
		}

		docStats.put(DocumentStatsAttribute.MAX_TF.toString(), freqOfMostFreqWord);
		docStats.put(DocumentStatsAttribute.TOTAL_WORDS.toString(), noOfWords);
		docStats.put(DocumentStatsAttribute.DOC_LENGTH.toString(), docLength);
		docStatsService.addDocumentStats(documentId, docStats);
	}

	/**
	 * Block compress the dictionary based on the arguments given.
	 * 
	 * @param index
	 * @param blockSize
	 * @param doFrontCodeCompression
	 * @param encodingFormat
	 * @return
	 */

	@Override
	public CompressedDictionary blockCompressDictionary(Map<String, List<Posting>> index, int blockSize,
			boolean doFrontCodeCompression, EncodingFormat encodingFormat) {
		generateGaps(index);
		String compressedTerms = null;
		if (!doFrontCodeCompression)
			compressedTerms = generateCompressedTerms(index.keySet());
		CompressedBlock[] compressedBlocks = generateCompressedBlocks(index, blockSize, compressedTerms,
				doFrontCodeCompression, encodingFormat);
		CompressedDictionary compressedDictionary = new CompressedDictionary(compressedTerms, blockSize,
				compressedBlocks);
		return compressedDictionary;
	}

	private CompressedBlock[] generateCompressedBlocks(Map<String, List<Posting>> index, int blockSize,
			String compressedTerms, boolean doFrontCodeCompression, EncodingFormat encodingFormat) {
		CompressedBlock[] compressedBlocks = new CompressedBlock[index.size() / blockSize + 1];
		Iterator<Entry<String, List<Posting>>> iterator = index.entrySet().iterator();
		int currentTermPtr = 0;
		int blkCount = 0;
		while (iterator.hasNext()) {
			BitSet[] freqBitSet = new BitSet[blockSize];
			Integer blkTermPtr = null;
			String frontCodedTerms = null;

			// Block Term pointer is not needed for front code compression.
			// Update it, only if the front code compression is not requested
			if (!doFrontCodeCompression)
				blkTermPtr = currentTermPtr;

			List<List<CompressedPosting>> blockCompressedPosting = new LinkedList<List<CompressedPosting>>();

			// Creating an ArrayList with the size of the block, so that there
			// won't be a need for resizing. ArrayList is also better for
			// indexed based access, which is used in front coding.
			List<String> termsInBlock = new ArrayList<String>(blockSize);

			for (int i = 0; i < blockSize && iterator.hasNext(); i++) {
				Entry<String, List<Posting>> entry = iterator.next();
				String term = entry.getKey();

				List<Posting> postings = entry.getValue();
				freqBitSet[i] = encoder.encodeValue(postings.size(), encodingFormat);
				List<CompressedPosting> compressedPostings = new LinkedList<CompressedPosting>();
				for (Posting posting : postings) {
					CompressedPosting compressedPosting = new CompressedPosting(
							encoder.encodeValue(posting.getGap(), encodingFormat),
							encoder.encodeValue(posting.getTermFrequency(), encodingFormat));
					compressedPostings.add(compressedPosting);
				}
				blockCompressedPosting.add(compressedPostings);

				if (doFrontCodeCompression) {
					termsInBlock.add(term);
				} else {
					// Move past term length
					currentTermPtr++;
					checkIfTermsMatch(compressedTerms, currentTermPtr, term);

					// Move to the next term
					currentTermPtr += term.length();
				}
			}
			// Generate front coded terms, if required
			if (doFrontCodeCompression)
				frontCodedTerms = StringUtil.generateFrontCodedTerms(termsInBlock);

			compressedBlocks[blkCount++] = new CompressedBlock(freqBitSet, blockCompressedPosting, blkTermPtr,
					frontCodedTerms);
		}
		return compressedBlocks;
	}

	private void checkIfTermsMatch(String compressedTerms, int currentTermPtr, String term) {
		// Avoiding the use of substrings, as they will generate copies and
		// cause additional memory overhead.
		for (int i = 0; i < term.length(); i++) {
			if (compressedTerms.charAt(i + currentTermPtr) != term.charAt(i)) {
				logger.error("Term: {} is not matching with the current index in the compressed terms.", term);
				break;
			}
		}
	}

	/**
	 * Generates compressed terms of the format <Length><Term> <Length> is
	 * stored as character value. So, it occupies 1 byte and can store length
	 * value up to 127. As we are not dealing with terms of length more than
	 * 127, we can optimize on space.
	 * 
	 * @param terms
	 * @return String of compressed terms in the mentioned format
	 */
	private String generateCompressedTerms(Set<String> terms) {
		StringBuilder builder = new StringBuilder();
		terms.forEach(term -> {
			builder.append(Character.valueOf((char) term.length())).append(term);
		});
		return builder.toString();

	}

	private void generateGaps(Map<String, List<Posting>> index) {
		for (Entry<String, List<Posting>> entry : index.entrySet()) {
			List<Posting> postingsList = entry.getValue();
			Posting firstPosting = postingsList.get(0);
			int prevDocId = firstPosting.getDocumentId();
			firstPosting.setGap(firstPosting.getDocumentId());
			for (int i = 1; i < postingsList.size(); i++) {
				Posting posting = postingsList.get(i);
				Integer docId = posting.getDocumentId();
				posting.setGap((docId - prevDocId));
				prevDocId = docId;
			}
		}
	}

}
