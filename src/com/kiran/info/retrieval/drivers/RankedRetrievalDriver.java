package com.kiran.info.retrieval.drivers;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kiran.info.retrieval.api.IDocumentStats;
import com.kiran.info.retrieval.api.IEncoder;
import com.kiran.info.retrieval.api.IIndexer;
import com.kiran.info.retrieval.api.ILemmatizer;
import com.kiran.info.retrieval.api.IRankedRetrievalService;
import com.kiran.info.retrieval.api.IStopWordDetector;
import com.kiran.info.retrieval.api.ITokenizer;
import com.kiran.info.retrieval.model.Posting;
import com.kiran.info.retrieval.model.Query;
import com.kiran.info.retrieval.model.RankedResult;
import com.kiran.info.retrieval.model.WeightingFunction;
import com.kiran.info.retrieval.services.DocumentStatsService;
import com.kiran.info.retrieval.services.EncodingService;
import com.kiran.info.retrieval.services.IndexerService;
import com.kiran.info.retrieval.services.LemmatizerService;
import com.kiran.info.retrieval.services.RankedRetrievalService;
import com.kiran.info.retrieval.services.StopWordService;
import com.kiran.info.retrieval.services.TokenizerService;
import com.kiran.info.retrieval.util.QueryParser;

/**
 * @author KiranKumar
 *
 */
public class RankedRetrievalDriver {

	private static final Logger logger = LogManager.getLogger(RankedRetrievalDriver.class);

	private static String directoryPath = null;
	private static String stopWordsFilePath = null;
	private static String queriesFilePath = null;

	public static void main(String[] args) {
		Scanner scanner = null;
		try {
			scanner = new Scanner(System.in);
			if (directoryPath == null) {
				System.out.println("Enter the absolute path of the directory containing documents: ");
				directoryPath = scanner.nextLine();
			}

			if (stopWordsFilePath == null) {
				System.out.println("Enter the absolute path of the file containing stop words: ");
				stopWordsFilePath = scanner.nextLine();
			}

			if (queriesFilePath == null) {
				System.out.println("Enter the absolute path of the file containing queries: ");
				queriesFilePath = scanner.nextLine();
			}
			ITokenizer tokenizer = new TokenizerService();
			IStopWordDetector stopWordDetector = new StopWordService(stopWordsFilePath);
			IEncoder encoder = new EncodingService();
			IDocumentStats indexStatsService = new DocumentStatsService(encoder);
			ILemmatizer lemmatizer = new LemmatizerService();
			IIndexer indexer = new IndexerService(tokenizer, lemmatizer, stopWordDetector, indexStatsService, encoder);
			QueryParser queryParser = new QueryParser(lemmatizer, stopWordDetector);

			List<Query> queries = queryParser.parseQueryFile(queriesFilePath);
			Map<String, List<Posting>> index = indexer.generateIndex(directoryPath, false, true);

			IRankedRetrievalService rankedRetrievalService = new RankedRetrievalService(index, indexStatsService);
			int queryNo = 1;
			for (Query query : queries) {
				logger.info(
						"\n\n----------------------------------------------Results for query number:{}----------------------------------------------",
						queryNo++);
				logger.info("Query words: {}", query.getWordVsFrequency().keySet());
				logger.info("Result using weighting function 1");
				fetchAndPrintResults(rankedRetrievalService, query, WeightingFunction.WF1, 5);
				logger.info("\nResult using weighting function 2");
				fetchAndPrintResults(rankedRetrievalService, query, WeightingFunction.WF2, 5);
				logger.info(
						"\n\n----------------------------------------------------------------------------------------------------------------------------");
			}

		} catch (Exception e) {
			logger.error("Exception encountered", e);
		}
	}

	private static void fetchAndPrintResults(IRankedRetrievalService rankedRetrievalService, Query query,
			WeightingFunction weightingFunction, int limit) {
		RankedResult result = rankedRetrievalService.fetchResults(query, weightingFunction, limit);
		logger.info("Query Vectors:{}", result.getQueryVectors());
		logger.info("Document vectors:{}", result.getDocVectors());
	}

}
