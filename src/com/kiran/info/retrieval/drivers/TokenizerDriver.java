package com.kiran.info.retrieval.drivers;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kiran.info.retrieval.api.ITokenizer;
import com.kiran.info.retrieval.model.Token;
import com.kiran.info.retrieval.services.TokenizerService;
import com.kiran.info.retrieval.util.DirectoryReader;
import com.kiran.info.retrieval.util.Stemmer;

public class TokenizerDriver {

	private static final Logger logger = LogManager.getLogger(TokenizerDriver.class);

	private int tokensWithFreqOneCount = 0;
	private int totalCount = 0;
	private int noOfDocuments = 0;

	public static void main(String[] args) {
		TokenizerDriver driver = new TokenizerDriver();
		Scanner scanner = new Scanner(System.in);
		try {
			logger.info("Enter the absolute path of the directory containing the documents: ");
			String path = scanner.nextLine();
			driver.noOfDocuments = DirectoryReader.getFileCountInDirectory(path);
			long startTime = System.nanoTime();
			Map<String, Integer> tokensVsFrequency = driver.tokenizeDocuments(path);
			long endTime = System.nanoTime() - startTime;
			logger.info("\n\n-------------------\nTokens statistics\n-------------------");
			logger.info("Time taken to tokenize the document collection in milliseconds: {}", endTime / 1000000);
			driver.computeMetrics(tokensVsFrequency, "tokens");
			logger.info("\n\n-------------------\nStems statistics\n-------------------");
			startTime = System.nanoTime();
			Map<String, Integer> stemmedTokensVsFrequency = Stemmer.stemTokens(tokensVsFrequency);
			endTime = System.nanoTime() - startTime;
			logger.info("Time taken to stem the token collection in milliseconds: {}", endTime / 1000000);
			driver.computeMetrics(stemmedTokensVsFrequency, "stems");
		} catch (Exception e) {
			logger.error("Exception during tokenization.", e);
		} finally {
			scanner.close();
		}
	}

	private void computeMetrics(Map<String, Integer> tokensVsFrequency, String attribute) {
		List<Token> tokensCollection = createTokenList(tokensVsFrequency);
		tokensCollection.sort((Token t1, Token t2) -> t2.getFrequency() - t1.getFrequency());
		logger.info("Total no. of {}: {}", attribute, totalCount);
		logger.info("No. of distinct {}: {}", attribute, tokensCollection.size());
		logger.info("No. of {} that occurs only once: {}", attribute, tokensWithFreqOneCount);
		logger.info("Average no. of {} per document based on frequency: {}", attribute, totalCount / noOfDocuments);
		logger.info("Average no. of distinct {} per document: {}", attribute, tokensCollection.size() / noOfDocuments);
		logger.info("-------Top 30-------");
		printTopKTokens(tokensCollection, 30);
	}

	private void printTopKTokens(List<Token> tokens, int k) {
		Iterator<Token> iterator = tokens.iterator();
		while (iterator.hasNext() && k > 0) {
			logger.info(iterator.next());
			k--;
		}
	}

	private List<Token> createTokenList(Map<String, Integer> tokensVsFrequency) {
		tokensWithFreqOneCount = 0;
		totalCount = 0;
		List<Token> tokensCollection = new LinkedList<Token>();
		tokensVsFrequency.forEach((word, frequency) -> {
			Token token = new Token(word, frequency);
			tokensCollection.add(token);
			totalCount += frequency;
			if (token.getFrequency().equals(1))
				tokensWithFreqOneCount++;
		});
		return tokensCollection;
	}

	private Map<String, Integer> tokenizeDocuments(String path) {
		ITokenizer tokenizer = new TokenizerService();
		return tokenizer.tokenizeDocumentsInDirectory(path);
	}

}
