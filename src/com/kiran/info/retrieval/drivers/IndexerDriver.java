package com.kiran.info.retrieval.drivers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kiran.info.retrieval.api.IDocumentStats;
import com.kiran.info.retrieval.api.IEncoder;
import com.kiran.info.retrieval.api.IIndexer;
import com.kiran.info.retrieval.api.ILemmatizer;
import com.kiran.info.retrieval.api.IStopWordDetector;
import com.kiran.info.retrieval.api.ITokenizer;
import com.kiran.info.retrieval.model.CompressedDictionary;
import com.kiran.info.retrieval.model.DocumentStatsAttribute;
import com.kiran.info.retrieval.model.EncodingFormat;
import com.kiran.info.retrieval.model.Posting;
import com.kiran.info.retrieval.services.DocumentStatsService;
import com.kiran.info.retrieval.services.EncodingService;
import com.kiran.info.retrieval.services.IndexerService;
import com.kiran.info.retrieval.services.LemmatizerService;
import com.kiran.info.retrieval.services.StopWordService;
import com.kiran.info.retrieval.services.TokenizerService;
import com.kiran.info.retrieval.util.SerializerUtil;

public class IndexerDriver {

	private static final Logger logger = LogManager.getLogger(IndexerDriver.class);

	private static String directoryPath = null;
	private static String stopWordsFilePath = null;

	public static void main(String[] args) {
		Scanner scanner = null;
		try {
			scanner = new Scanner(System.in);
			if (directoryPath == null) {
				System.out.println("Enter the absolute path of the directory containing documents: ");
				directoryPath = scanner.nextLine();
			}

			if (stopWordsFilePath == null) {
				System.out.println("Enter the absolute path of the file containing stop words: ");
				stopWordsFilePath = scanner.nextLine();
			}
			ITokenizer tokenizer = new TokenizerService();
			IStopWordDetector stopWordDetector = new StopWordService(stopWordsFilePath);
			IEncoder encoder = new EncodingService();
			IDocumentStats indexV1StatsService = new DocumentStatsService(encoder);
			IDocumentStats indexV2StatsService = new DocumentStatsService(encoder);
			ILemmatizer lemmatizer = new LemmatizerService();
			IIndexer indexerV1 = new IndexerService(tokenizer, lemmatizer, stopWordDetector, indexV1StatsService,
					encoder);
			IIndexer indexerV2 = new IndexerService(tokenizer, lemmatizer, stopWordDetector, indexV2StatsService,
					encoder);

			// Generate Index V1
			long startTime = System.nanoTime();
			Map<String, List<Posting>> indexVersion1 = indexerV1.generateIndex(directoryPath, false, true);
			logger.info("Time taken in seconds to build index V1 uncompressed: {}",
					(System.nanoTime() - startTime) / 1000000000);

			// Generate Index V2
			startTime = System.nanoTime();
			Map<String, List<Posting>> indexVersion2 = indexerV2.generateIndex(directoryPath, true, false);
			logger.info("Time taken in seconds to build index V2 uncompressed: {}",
					(System.nanoTime() - startTime) / 1000000000);

			// Block compress Index V1
			startTime = System.nanoTime();
			CompressedDictionary blockCompressedIndexV1 = indexerV1.blockCompressDictionary(indexVersion1, 8, false,
					EncodingFormat.GAMMA);
			logger.info("Time taken in milliseconds to compress index V1: {}",
					(System.nanoTime() - startTime) / 1000000);

			// Block compress Index V2 with front coding
			startTime = System.nanoTime();
			CompressedDictionary blockCompressedIndexV2 = indexerV1.blockCompressDictionary(indexVersion2, 8, true,
					EncodingFormat.DELTA);
			logger.info("Time taken in milliseconds to compress index V2: {}",
					(System.nanoTime() - startTime) / 1000000);

			queryIndexes(indexVersion1, indexVersion2, blockCompressedIndexV1, blockCompressedIndexV2,
					indexV1StatsService, indexV2StatsService, encoder);
			serializeIndexes(indexV1StatsService, indexV2StatsService, indexVersion1, indexVersion2,
					blockCompressedIndexV1, blockCompressedIndexV2);
			logger.info("Completed the execution");
		} catch (Exception e) {
			logger.error("Exception encountered!", e);
		} finally {
			scanner.close();
		}
	}

	private static void queryIndexes(Map<String, List<Posting>> indexVersion1, Map<String, List<Posting>> indexVersion2,
			CompressedDictionary blockCompressedIndexV1, CompressedDictionary blockCompressedIndexV2,
			IDocumentStats indexV1StatsService, IDocumentStats indexV2StatsService, IEncoder encoder)
			throws IOException {
		logger.info("No. of inverted lists in index V1: {}", indexVersion1.size());
		logger.info("No. of inverted lists in index V2: {}", indexVersion2.size());
		List<String> terms1 = Arrays.asList("Reynolds", "NASA", "Prandtl", "flow", "pressure", "boundary", "shock");
		List<String> terms2 = Arrays.asList("Reynold", "NASA", "Prandtl", "flow", "pressur", "boundari", "shock");
		logger.info("\n\n--------------Term stats for Index version 1--------------");
		fetchStatsFromIndexes(indexVersion1, terms1);
		logger.info("\n\n--------------Term stats for Index version 2--------------");
		fetchStatsFromIndexes(indexVersion2, terms2);

		logger.info("\n\n--------------Stats for NASA in Index V1--------------");
		printStatsForNasa(indexVersion1, indexV1StatsService);
		logger.info("\n\n--------------Stats for NASA in Index V2--------------");
		printStatsForNasa(indexVersion2, indexV1StatsService);

		logger.info("\n\n--------------Document Frequency stats for Index version 1--------------");
		printTermsWithLargestandLowestDF(indexVersion1);
		logger.info("\n\n--------------Document Frequency stats for Index version 2--------------");
		printTermsWithLargestandLowestDF(indexVersion2);
		logger.info("\n\n--------------Document stats--------------");
		printDocIdsForMaxTFandDocLength(indexV1StatsService);
	}

	private static void fetchStatsFromIndexes(Map<String, List<Posting>> indexVersion1, List<String> terms1)
			throws IOException {
		int tf;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		for (String term : terms1) {
			term = term.toLowerCase();
			bos.reset();
			if (indexVersion1.containsKey(term)) {
				List<Posting> postingList = indexVersion1.get(term);
				tf = 0;
				for (Posting posting : postingList) {
					tf += posting.getTermFrequency();
				}
				oos.writeObject(postingList);
				oos.flush();
				logger.info("Term: {} DF:{} TF:{} Inverted list length in bytes:{}", term, postingList.size(), tf,
						bos.toByteArray().length);
			} else {
				logger.error("Unable to find term:{} in both indexes", term);
			}
		}
	}

	private static void printStatsForNasa(Map<String, List<Posting>> indexVersion1,
			IDocumentStats indexV1StatsService) {
		List<Posting> postings = indexVersion1.get("nasa");
		logger.info("DF: {}", postings.size());
		for (int i = 0; i < 3; i++) {
			Posting posting = postings.get(i);
			Integer documentId = posting.getDocumentId();
			Map<String, Object> stats = indexV1StatsService.getStatsForDocumentId(documentId);
			logger.info("DocId:{} TF:{} DocLen:{} MaxTF:{}", documentId, posting.getTermFrequency(),
					stats.get(DocumentStatsAttribute.TOTAL_WORDS.toString()),
					stats.get(DocumentStatsAttribute.MAX_TF.toString()));
		}
	}

	private static void printTermsWithLargestandLowestDF(Map<String, List<Posting>> indexVersion) {
		List<String> termsWithOneDF = new LinkedList<String>();
		List<String> termsWithMaxDF = new LinkedList<String>();
		int maxDf = 1;
		for (Entry<String, List<Posting>> entry : indexVersion.entrySet()) {
			List<Posting> postings = entry.getValue();
			String term = entry.getKey();
			if (postings.size() == 1)
				termsWithOneDF.add(term);
			if (postings.size() > maxDf) {
				termsWithMaxDF = new LinkedList<String>();
				termsWithMaxDF.add(term);
				maxDf = postings.size();
			} else if (postings.size() == maxDf)
				termsWithMaxDF.add(term);
		}
		logger.info("Max DF:{}. Terms:{}", maxDf, termsWithMaxDF);
		logger.info("Lowest DF:{}. Terms:{}", 1, termsWithOneDF);
	}

	private static void printDocIdsForMaxTFandDocLength(IDocumentStats indexV1StatsService) {
		int tf;
		int docIdWithMaxTf = -1;
		int maxTf = 0;

		int docIdWithMaxDocLen = -1;
		int maxDocLen = 0;
		for (Entry<Integer, Map<String, Object>> entry : indexV1StatsService.getAllDocumentStats().entrySet()) {
			Integer docId = entry.getKey();
			Map<String, Object> attributes = entry.getValue();
			tf = (Integer) attributes.get(DocumentStatsAttribute.MAX_TF.toString());
			int docLen = (Integer) attributes.get(DocumentStatsAttribute.TOTAL_WORDS.toString());
			if (tf > maxTf) {
				docIdWithMaxTf = docId;
				maxTf = tf;
			}
			if (docLen > maxDocLen) {
				docIdWithMaxDocLen = docId;
				maxDocLen = docLen;
			}
		}

		logger.info("Document with largest Max TF: {} in the collection", docIdWithMaxTf);
		logger.info("Document with largest Doc length: {} in the collection", docIdWithMaxDocLen);
	}

	private static void serializeIndexes(IDocumentStats indexV1StatsService, IDocumentStats indexV2StatsService,
			Map<String, List<Posting>> indexVersion1, Map<String, List<Posting>> indexVersion2,
			CompressedDictionary blockCompressedIndexV1, CompressedDictionary blockCompressedIndexV2) {
		SerializerUtil.serializeObjects("Index_Version1.uncompress", indexVersion1,
				indexV1StatsService.getAllDocumentStats());
		SerializerUtil.serializeObjects("Index_Version2.uncompress", indexVersion2,
				indexV2StatsService.getAllDocumentStats());
		SerializerUtil.serializeObjects("Index_Version1.compressed", blockCompressedIndexV1,
				indexV1StatsService.getCompressedStats(EncodingFormat.GAMMA));
		SerializerUtil.serializeObjects("Index_Version2.compressed", blockCompressedIndexV2,
				indexV2StatsService.getCompressedStats(EncodingFormat.DELTA));
	}

}
