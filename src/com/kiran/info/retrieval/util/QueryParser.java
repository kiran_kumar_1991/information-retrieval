package com.kiran.info.retrieval.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.kiran.info.retrieval.api.ILemmatizer;
import com.kiran.info.retrieval.api.IStopWordDetector;
import com.kiran.info.retrieval.model.Query;

/**
 * @author KiranKumar
 *
 */
public class QueryParser {

	private static final String queryNumberRegex = "Q[0-9]+[:]";

	private ILemmatizer lemmatizer;
	private IStopWordDetector stopWordDetector;

	public QueryParser(ILemmatizer lemmatizer, IStopWordDetector stopWordDetector) {
		super();
		this.lemmatizer = lemmatizer;
		this.stopWordDetector = stopWordDetector;
	}

	/**
	 * Parses the given query file and returns a list of queries
	 * 
	 * @param absolutePath
	 * @return List of queries
	 * @throws IOException
	 */
	public List<Query> parseQueryFile(String absolutePath) throws IOException {
		List<Query> queries = new ArrayList<Query>();
		List<String> lines = FileReader.readAllLines(absolutePath);
		StringBuilder builder = new StringBuilder();
		Iterator<String> linesIterator = lines.iterator();
		while (linesIterator.hasNext()) {
			String line = linesIterator.next();
			if (line.matches(queryNumberRegex)) {
				continue;
			} else if (StringUtil.isEmpty(line) || !linesIterator.hasNext()) {
				List<String> lemmas = lemmatizer.lemmatize(builder.toString());
				Iterator<String> iterator = lemmas.iterator();

				// Remove stop words in the query
				while (iterator.hasNext()) {
					String lemma = iterator.next();
					if (stopWordDetector.isStopWord(lemma) || !lemma.matches("[\\w]+[.]*[\\w]*"))
						iterator.remove();
				}
				Query query = new Query(lemmas);
				queries.add(query);

				// Clear the contents of the StringBuilder, after reaching the
				// end of the query
				builder.delete(0, builder.length());
			} else {
				builder.append(line).append(" ");
			}
		}
		return queries;
	}

	public static void main(String[] args) {
		String line = "Q3:";
		if (line.matches(queryNumberRegex))
			System.out.println("True");
		else
			System.out.println("False");
	}

}
