package com.kiran.info.retrieval.util;

import java.util.Collection;

import com.kiran.info.retrieval.model.Vector;

public class WeightingFunctionUtil {

	/**
	 * Uses Weighting Function Type 1
	 * 
	 * @param termFreq
	 * @param maxTermFreq
	 * @param collectionSize
	 * @param docFreq
	 * @return
	 */
	public static double computeWeightedValueUsingType1(int termFreq, int maxTermFreq, int collectionSize,
			int docFreq) {
		return (0.4 + 0.6 * Math.log(termFreq + 0.5) / Math.log(maxTermFreq + 1.0))
				* (Math.log(collectionSize / docFreq) / Math.log(collectionSize));

	}

	/**
	 * Uses Weighting Function Type 2
	 * 
	 * @param termFreq
	 * @param collectionSize
	 * @param docFreq
	 * @param avgDocLength
	 * @param docLength
	 * @return
	 */
	public static double computeWeightedValueUsingType2(int termFreq, int collectionSize, int docFreq,
			float avgDocLength, int docLength) {
		return (0.4 + 0.6 * (termFreq / (termFreq + 0.5 + 1.5 * (docLength / avgDocLength)))
				* Math.log(collectionSize / docFreq) / Math.log(collectionSize));
	}

	/**
	 * Computes the normalized value of each vector and sets it.
	 * 
	 * @param vectors
	 */
	public static void computeAndSetNormalizedValues(Collection<Vector> vectors) {
		double totalValue = 0d;
		for (Vector vector : vectors) {
			totalValue += Math.pow(vector.getWeightedValue(), 2);
		}
		double totalVectorLength = Math.sqrt(totalValue);

		for (Vector vector : vectors) {
			vector.setNormalizedValue(vector.getWeightedValue() / totalVectorLength);
		}
	}
}
