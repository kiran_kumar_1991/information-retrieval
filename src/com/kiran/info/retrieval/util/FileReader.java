package com.kiran.info.retrieval.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author KiranKumar
 *
 */
public class FileReader {

	private static final Logger logger = LogManager.getLogger(FileReader.class);
	private static final String TITLE_TAG = "<TITLE>";

	/**
	 * Reads all the lines of the file into memory before returning it. Optimal
	 * for files of smaller size and is not recommended for large files.
	 * 
	 * @param absolutePath
	 * @return List<String>
	 * @throws IOException
	 */
	public static List<String> readAllLines(String absolutePath) throws IOException {
		Path path = Paths.get(absolutePath);
		return Files.readAllLines(path);
	}

	/**
	 * Reads all the lines of the file into memory and concatenates them before
	 * returning it. Optimal for files of smaller size and is not recommended
	 * for large files.
	 * 
	 * @param absolutePath
	 * @return List<String>
	 * @throws IOException
	 */
	public static String readAllLinesAndConcatenate(String absolutePath) throws IOException {
		Path path = Paths.get(absolutePath);
		List<String> lines = Files.readAllLines(path);
		StringBuilder builder = new StringBuilder();
		lines.forEach(line -> builder.append(line));
		return builder.toString();
	}

	/**
	 * Read the title of the document enclosed in the Title tag.
	 * 
	 * @param absolutePath
	 * @return Document Title
	 * @throws IOException
	 */
	public static String getDocumentTitle(String absolutePath) {
		File file = new File(absolutePath);
		FileInputStream fis = null;
		InputStreamReader inputStreamReader = null;
		BufferedReader br = null;
		String line = null;
		try {
			fis = new FileInputStream(file);
			inputStreamReader = new InputStreamReader(fis);
			br = new BufferedReader(inputStreamReader);
			while ((line = br.readLine()) != null) {
				if (!StringUtil.isEmpty(line) && line.equals(TITLE_TAG))
					return br.readLine();
			}
		} catch (Exception e) {
			logger.error("Exception in reading document title", e);
		} finally {
			try {
				br.close();
				inputStreamReader.close();
				fis.close();
			} catch (IOException e) {
				logger.error("Exception in reading document title", e);
			}
		}
		return null;
	}

}
