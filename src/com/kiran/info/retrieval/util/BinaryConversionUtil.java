package com.kiran.info.retrieval.util;

import java.util.BitSet;
import java.util.List;

/**
 * @author KiranKumar
 *
 */
public class BinaryConversionUtil {

	/**
	 * Converts the given binary string to BitSet object
	 * 
	 * @param binaryString
	 * @return BitSet
	 */
	public static BitSet convertBinaryStringToBitSet(String binaryString) {
		BitSet result = new BitSet(binaryString.length());
		for (int i = binaryString.length() - 1; i >= 0; i--) {
			if (binaryString.charAt(i) == '1')
				result.set(i);
		}
		return result;
	}

	/**
	 * Converts the BitSet object to binary string
	 * 
	 * @param input
	 * @return String
	 */
	public static String convertBitSetToBinaryString(BitSet input) {
		StringBuilder builder = new StringBuilder();
		for (int i = input.length() - 1; i >= 0; i--)
			builder.append(input.get(i) == true ? 1 : 0);
		return builder.toString();
	}

	/**
	 * Converts the given binary string into a boolean array
	 * 
	 * @param binaryString
	 * @return boolean[]
	 */
	public static boolean[] convertBinaryStringToBooleanArray(String binaryString) {
		boolean[] result = new boolean[binaryString.length()];
		for (int i = 0; i < binaryString.length(); i++) {
			if (binaryString.charAt(i) == '1')
				result[i] = true;
		}
		return result;
	}

	/**
	 * Converts boolean array to BitSet object
	 * 
	 * @param input
	 * @return BitSet
	 */
	public static BitSet convertBooleanToBitSet(boolean[] input) {
		BitSet result = new BitSet(input.length);
		for (int i = input.length - 1, j = 0; i >= 0; i--, j++)
			if (input[j] == true)
				result.set(i);
		return result;
	}

	/**
	 * Converts BitSet input to boolean array
	 * 
	 * @param input
	 * @return boolean[]
	 */
	public static boolean[] convertBitSetToBoolean(BitSet input) {
		boolean[] result = new boolean[input.length()];
		for (int i = input.length() - 1, j = 0; i >= 0; i--, j++)
			if (input.get(i) == true)
				result[j] = true;
		return result;
	}

	/**
	 * Merges an array of bitset objects into a single object
	 * 
	 * @param bitSets
	 * @return BitSet
	 */
	public static BitSet mergeBitSetArray(BitSet[] bitSets) {
		int totalSize = 0;
		for (BitSet set : bitSets) {
			if (set != null)
				totalSize += set.length();
		}
		BitSet result = new BitSet(totalSize);
		int index = 0;
		for (BitSet set : bitSets) {
			if (set != null) {
				for (int i = 0; i < set.length(); i++, index++) {
					if (set.get(i) == true)
						result.set(index);
				}
			}
		}
		return result;
	}

	/**
	 * Merges multiple bitset objects into a single object
	 * 
	 * @param bitSets
	 * @return BitSet
	 */
	public static BitSet mergeBitSets(BitSet... bitSets) {
		return mergeBitSetArray(bitSets);
	}

	/**
	 * Merges list of BitSet objects into a single object
	 * 
	 * @param bitSets
	 * @return BitSet
	 */
	public static BitSet mergeBitSetList(List<BitSet> bitSets) {
		return mergeBitSetArray(bitSets.toArray(new BitSet[bitSets.size()]));
	}

}
