package com.kiran.info.retrieval.util;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author KiranKumar
 *
 */
public class DocumentIdGenerator {

	private static AtomicInteger docId = new AtomicInteger(0);

	/**
	 * Generates a unique document ID, which will be monotonically increasing.
	 * Guaranteed to be thread-safe.
	 * 
	 * @return int
	 */
	public static int getDocumentId() {
		return docId.incrementAndGet();
	}

	/**
	 * Resets the internal document ID generating counter to 0.
	 */
	public static void resetCounter() {
		docId.set(0);
	}

}
