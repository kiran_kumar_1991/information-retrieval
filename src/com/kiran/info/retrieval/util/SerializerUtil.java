package com.kiran.info.retrieval.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author KiranKumar
 *
 */
public class SerializerUtil {

	public static final Logger logger = LogManager.getLogger(SerializerUtil.class);

	/**
	 * Serializes the given objects to the absolute path given.
	 * 
	 * @param absolutePath
	 * @param objects
	 */
	public static void serializeObjects(String absolutePath, Object... objects) {
		ObjectOutputStream objectOutputStream = null;
		try {
			File file = new File(absolutePath);
			if (file.exists())
				file.delete();
			FileOutputStream fos = new FileOutputStream(file);
			objectOutputStream = new ObjectOutputStream(fos);
			for (Object obj : objects)
				objectOutputStream.writeObject(obj);
		} catch (IOException e) {
			logger.error("Exception during serialization of the object.", e);
		} finally {
			try {
				objectOutputStream.close();
			} catch (IOException e) {
				logger.error("Exception during serialization of the object.", e);
			}
		}

	}
}
