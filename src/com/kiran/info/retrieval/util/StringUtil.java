package com.kiran.info.retrieval.util;

import java.util.Arrays;
import java.util.List;

/**
 * @author KiranKumar
 *
 */
public class StringUtil {

	/**
	 * Removes the XML tags in the given line
	 * 
	 * @param inputLine
	 * @return inputLine without XML tags
	 */
	public static String removeTags(String line) {
		if (line != null)
			return line.replaceAll("(<([^>])+>)", "");
		return "";
	}

	/**
	 * Checks if the given String is either null or empty
	 * 
	 * @param String
	 * @return boolean
	 */
	public static boolean isEmpty(String content) {
		if (content == null || content.isEmpty())
			return true;
		return false;
	}

	/**
	 * Generates front code for the list of terms to save space. For example,
	 * the front code for the list of terms {"committee", "common", "commonly",
	 * "communicate", "communication"} is generated as
	 * comm*ittee5◊on2◊only4◊unicate7◊unication9◊ --> Here, 'comm' is common
	 * to all the words and we can save space by using a common prefix. '*'
	 * indicates the end of the prefix and is followed by the rest of the
	 * characters with the size at the end. '◊' is used to separate the words.
	 * 
	 * Limitation: Current approach tries to generate prefix with the first term
	 * in the block only! It would be inefficient for the cases where the first
	 * term does not share a prefix with the rest of the terms in the block
	 * 
	 * @param terms
	 * @return
	 */
	public static String generateFrontCodedTerms(List<String> terms) {
		String firstTerm = terms.get(0);
		boolean flag = true;
		int frontCodeSize = 0, i, j;
		for (i = 0; i < firstTerm.length() && flag; i++, frontCodeSize++) {
			for (j = 1; j < terms.size(); j++) {
				if (terms.get(j).charAt(i) != firstTerm.charAt(i)) {
					flag = false;
					break;
				}
			}
		}

		StringBuilder builder = new StringBuilder();

		builder.append(((char) frontCodeSize));
		for (i = 0; i < frontCodeSize - 1; i++) {
			// Avoiding the use of substring, as it increases memory overhead by
			// creating a copy of the actual characters
			builder.append(firstTerm.charAt(i));
		}
		builder.append('*');
		for (String term : terms) {
			for (i = frontCodeSize - 1; i < term.length(); i++) {
				builder.append(term.charAt(i));
			}
			builder.append(((char) (term.length() - frontCodeSize + 1)));
			builder.append('◊');
		}

		return builder.toString();
	}

	public static void main(String[] args) {
		List<String> terms = Arrays.asList("committee", "common", "commonly", "communicate", "cot");
		String frontCodedTerms = generateFrontCodedTerms(terms);
		System.out.println(frontCodedTerms);
		System.out.println(frontCodedTerms.length());
	}
}
