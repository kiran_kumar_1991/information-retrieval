package com.kiran.info.retrieval.util;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * @author KiranKumar
 *
 */
public class DirectoryReader {

	/**
	 * Returns the list of files in the directory
	 * 
	 * @param path
	 * @return List<File>
	 */
	public static List<File> getFilesInDirectory(String path) {
		File directory = new File(path);
		File[] files = directory.listFiles();
		Arrays.sort(files);
		return Arrays.asList(files);
	}

	/**
	 * Gets the count of files in the directory
	 * 
	 * @param path
	 * @return int
	 */
	public static int getFileCountInDirectory(String path) {
		File directory = new File(path);
		return directory.listFiles().length;
	}
}
