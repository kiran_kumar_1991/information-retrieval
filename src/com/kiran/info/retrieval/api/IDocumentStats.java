package com.kiran.info.retrieval.api;

import java.util.Map;

import com.kiran.info.retrieval.model.EncodingFormat;

/**
 * @author KiranKumar
 *
 */
public interface IDocumentStats {

	/**
	 * Stores the statistics of the given documentId. It is merged with the
	 * existing statistics for the document, if present, and overwrites the same
	 * entries.
	 * 
	 * @param docId
	 * @param stats
	 */
	public void addDocumentStats(Integer docId, Map<String, Object> stats);

	/**
	 * Fetches the statistics for the given document Id.
	 * 
	 * @param docId
	 * @return Map<String, Object> statistics
	 */
	public Map<String, Object> getStatsForDocumentId(Integer docId);

	/**
	 * Print the statistics for all the documents
	 */
	public void printDocumentsStats();

	/**
	 * Returns the statistics of all documents in a Map. Map of documentId
	 * versus Map of Document attribute name against values
	 * 
	 * @return Map<Integer, Map<String, Object>> statistics
	 */
	public Map<Integer, Map<String, Object>> getAllDocumentStats();

	/**
	 * Returns the statistics of all documents in a Map. Map of documentId
	 * versus Map of Document attribute name against values. The attribute
	 * values are compressed based on the encoding format provided in the
	 * argument
	 * 
	 * @return Map<Integer, Map<String, byte[]>> statistics
	 */
	public Map<Integer, Map<String, byte[]>> getCompressedStats(EncodingFormat encodingFormat);

}
