package com.kiran.info.retrieval.api;

import java.io.File;
import java.util.Map;

/**
 * @author KiranKumar
 *
 */
public interface ITokenizer {

	/**
	 * Tokenizes the document given as argument
	 * 
	 * @param filePath
	 * @return Map of tokens versus their frequency
	 */
	public Map<String, Integer> tokenizeDocument(File filePath);

	/**
	 * Tokenizes all the documents in the given directory
	 * 
	 * @param directory
	 * @return Map of tokens versus their frequency
	 */
	public Map<String, Integer> tokenizeDocumentsInDirectory(String directoryPath);

}
