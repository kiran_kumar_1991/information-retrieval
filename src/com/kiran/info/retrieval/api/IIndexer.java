package com.kiran.info.retrieval.api;

import java.util.List;
import java.util.Map;

import com.kiran.info.retrieval.model.CompressedDictionary;
import com.kiran.info.retrieval.model.EncodingFormat;
import com.kiran.info.retrieval.model.Posting;

/**
 * @author KiranKumar
 *
 */
public interface IIndexer {

	/**
	 * Generates an index of all the documents contained in the specified
	 * directory path. Based on the arguments passed, it does stemming and
	 * lemmatization of the tokens, if needed.
	 * 
	 * @param directoryPath
	 * @param doStemming
	 * @param doLemmatization
	 * @return Map<String, List<Posting>> - Tokens against the Posting list
	 */
	public Map<String, List<Posting>> generateIndex(String directoryPath, boolean doStemming, boolean doLemmatization);

	/**
	 * Block compress the dictionary based on the arguments given.
	 * 
	 * @param index
	 * @param blockSize
	 * @param doFrontCodeCompression
	 * @param encodingFormat
	 * @return
	 */
	public CompressedDictionary blockCompressDictionary(Map<String, List<Posting>> index, int blockSize,
			boolean doFrontCodeCompression, EncodingFormat encodingFormat);

}
