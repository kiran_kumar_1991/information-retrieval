package com.kiran.info.retrieval.api;

import java.util.BitSet;

import com.kiran.info.retrieval.model.EncodingFormat;

/**
 * @author KiranKumar
 *
 */
public interface IEncoder {

	/**
	 * Encodes the given value based on the specified encoding format
	 * 
	 * @param value
	 * @param encodingFormat
	 * @return BitSet of the encoded value
	 */
	public BitSet encodeValue(int value, EncodingFormat encodingFormat);

}
