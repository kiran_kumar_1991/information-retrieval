package com.kiran.info.retrieval.api;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author KiranKumar
 *
 */
public interface ILemmatizer {
	/**
	 * Lemmatizes the document given as argument
	 * 
	 * @param filePath
	 * @return Map of tokens versus their frequency
	 */
	public Map<String, Integer> lemmatizeDocument(File filePath);

	/**
	 * Lemmatizes all the documents in the given directory
	 * 
	 * @param directory
	 * @return Map of tokens versus their frequency
	 */
	public Map<String, Integer> lemmatizeDocumentsInDirectory(String directoryPath);

	/**
	 * Lemmatizes the given text
	 * 
	 * @param documentText
	 * @return List of lemmas
	 */
	public List<String> lemmatize(String documentText);

}
