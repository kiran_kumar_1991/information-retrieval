package com.kiran.info.retrieval.api;

/**
 * @author KiranKumar
 *
 */
public interface IStopWordDetector {

	/**
	 * Checks if the given word is a stop word or not.
	 * 
	 * @param word
	 * @return boolean
	 */
	public boolean isStopWord(String word);

}
