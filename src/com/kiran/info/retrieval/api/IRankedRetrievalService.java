package com.kiran.info.retrieval.api;

import com.kiran.info.retrieval.model.Query;
import com.kiran.info.retrieval.model.RankedResult;
import com.kiran.info.retrieval.model.WeightingFunction;

public interface IRankedRetrievalService {

	/**
	 * Queries the constructed index using the given weighting function
	 * 
	 * @param query
	 * @param weightingFunction
	 * @param limit
	 * @return RankedResult object
	 */
	public RankedResult fetchResults(Query query, WeightingFunction weightingFunction, int limit);

}
