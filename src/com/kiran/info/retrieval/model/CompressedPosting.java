package com.kiran.info.retrieval.model;

import java.io.Serializable;
import java.util.BitSet;

public class CompressedPosting implements Serializable {

	private static final long serialVersionUID = -5754191371749921268L;
	private BitSet encodedGap;
	private BitSet encodedTermFrequency;

	public CompressedPosting(BitSet encodedGap, BitSet encodedTermFrequency) {
		super();
		this.encodedGap = encodedGap;
		this.encodedTermFrequency = encodedTermFrequency;
	}

	public BitSet getEncodedGap() {
		return encodedGap;
	}

	public BitSet getEncodedTermFrequency() {
		return encodedTermFrequency;
	}

}
