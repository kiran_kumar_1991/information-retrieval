package com.kiran.info.retrieval.model;

import java.io.Serializable;

public class Posting implements Serializable {

	private static final long serialVersionUID = 7882734427070965883L;
	private int documentId;
	private int termFrequency;
	private int gap;

	public Posting(int documentId, int termFrequency) {
		super();
		this.documentId = documentId;
		this.termFrequency = termFrequency;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public Integer getTermFrequency() {
		return termFrequency;
	}

	public Integer getGap() {
		return gap;
	}

	public void setGap(int gap) {
		this.gap = gap;
	}

}
