package com.kiran.info.retrieval.model;

import java.util.Collection;

public class RankedResult {

	private Collection<Vector> queryVectors;
	private Collection<DocumentResult> docVectors;

	public RankedResult(Collection<Vector> queryVectors, Collection<DocumentResult> docVectors) {
		super();
		this.queryVectors = queryVectors;
		this.docVectors = docVectors;
	}

	public Collection<Vector> getQueryVectors() {
		return queryVectors;
	}

	public Collection<DocumentResult> getDocVectors() {
		return docVectors;
	}

}
