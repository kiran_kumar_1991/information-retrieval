package com.kiran.info.retrieval.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Query {

	private List<String> words = new LinkedList<String>();
	private Map<String, Integer> countMap = new HashMap<String, Integer>();

	public Query(List<String> words) {
		this.words = words;
	}

	public List<String> getWords() {
		return words;
	}

	public void addWord(String word) {
		words.add(word);
	}

	public void addMultipleWords(List<String> newWords) {
		this.words.addAll(newWords);
	}

	public Map<String, Integer> getWordVsFrequency() {
		// Count the frequency of the words in the query. Lazy computation
		// method. Compute only when needed and do it only once at max.
		if (countMap.size() == 0) {
			words.forEach(word -> {
				if (!countMap.containsKey(word))
					countMap.put(word, 1);
				else
					countMap.put(word, countMap.get(word) + 1);
			});
		}
		return countMap;
	}

}
