
package com.kiran.info.retrieval.model;

import java.io.Serializable;

public enum DocumentStatsAttribute implements Serializable {

	MOST_FREQ_TERM, TOTAL_WORDS, MAX_TF, DOC_LENGTH, DOC_PATH, DOC_TITLE

}
