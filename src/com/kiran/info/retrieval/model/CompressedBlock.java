package com.kiran.info.retrieval.model;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;

import com.kiran.info.retrieval.util.BinaryConversionUtil;

public class CompressedBlock implements Serializable {

	private static final long serialVersionUID = 1998959318623103034L;
	private BitSet[] frequencies;
	private List<List<CompressedPosting>> blockCompressedPostings;
	private Integer termPointer;
	private String frontCodedTerms;

	public CompressedBlock(BitSet[] frequencies, List<List<CompressedPosting>> blockCompressedPostings,
			Integer termPointer, String frontCodedTerms) {
		super();
		this.frequencies = frequencies;
		this.blockCompressedPostings = blockCompressedPostings;
		this.termPointer = termPointer;
		this.frontCodedTerms = frontCodedTerms;
	}

	public BitSet[] getFrequencies() {
		return frequencies;
	}

	public int getTermPointer() {
		return termPointer;
	}

	public List<List<CompressedPosting>> getBlockCompressedPostings() {
		return blockCompressedPostings;
	}

	public String getFrontCodedTerms() {
		return frontCodedTerms;
	}

	/*
	 * BitSet is efficient in memory but causes storage overhead when
	 * serialized. So, it's recommended to convert BitSet to byte[] before
	 * serializing. The BitSets are merged to one large BitSet and is then
	 * converted into a single byte[] for maximum efficiency. This reduces
	 * unnecessary bit-padding to make sure that the bits are byte aligned.
	 */
	private void writeObject(ObjectOutputStream oos) throws IOException {
		byte[] freqBytes = BinaryConversionUtil.mergeBitSetArray(frequencies).toByteArray();
		oos.write(freqBytes);
		oos.writeObject(frontCodedTerms);
		for (List<CompressedPosting> termPostings : blockCompressedPostings) {
			List<BitSet> bitsetCollection = new LinkedList<BitSet>();

			// Merge compressed postings into a single BitSet and convert it to
			// byte[] for maximum space
			// efficiency
			for (CompressedPosting posting : termPostings) {
				bitsetCollection.add(posting.getEncodedGap());
				bitsetCollection.add(posting.getEncodedTermFrequency());
			}
			oos.write(BinaryConversionUtil.mergeBitSetList(bitsetCollection).toByteArray());
		}
	}

}
