package com.kiran.info.retrieval.model;

import com.kiran.info.retrieval.util.WeightingFunctionUtil;

public class Vector {

	private String term;
	private int termFreq;
	private int maxTermFreq;
	private int collectionSize;
	private int docFreq;
	private float avgDocLength;
	private int docLength;
	private WeightingFunction weightingFunction;
	private double weightedValue;
	private double normalizedValue;

	/**
	 * Computes vector value based on Weighting Function 1
	 * 
	 * @param term
	 * @param termFreq
	 * @param maxTermFreq
	 * @param collectionSize
	 * @param docFreq
	 */
	public Vector(String term, int termFreq, int maxTermFreq, int collectionSize, int docFreq) {
		super();
		this.term = term;
		this.termFreq = termFreq;
		this.maxTermFreq = maxTermFreq;
		this.collectionSize = collectionSize;
		this.docFreq = docFreq;
		this.weightingFunction = WeightingFunction.WF1;
		this.weightedValue = WeightingFunctionUtil.computeWeightedValueUsingType1(termFreq, maxTermFreq, collectionSize,
				docFreq);
	}

	/**
	 * Computes vector value based on Weighting Function 2
	 * 
	 * @param term
	 * @param termFreq
	 * @param collectionSize
	 * @param docFreq
	 * @param avgDocLength
	 * @param docLength
	 */
	public Vector(String term, int termFreq, int collectionSize, int docFreq, float avgDocLength, int docLength) {
		super();
		this.term = term;
		this.termFreq = termFreq;
		this.collectionSize = collectionSize;
		this.docFreq = docFreq;
		this.avgDocLength = avgDocLength;
		this.docLength = docLength;
		this.weightingFunction = WeightingFunction.WF2;
		this.weightedValue = WeightingFunctionUtil.computeWeightedValueUsingType2(termFreq, collectionSize, docFreq,
				avgDocLength, docLength);
	}

	public int getTermFreq() {
		return termFreq;
	}

	public int getMaxTermFreq() {
		return maxTermFreq;
	}

	public int getCollectionSize() {
		return collectionSize;
	}

	public int getDocFreq() {
		return docFreq;
	}

	public float getAvgDocLength() {
		return avgDocLength;
	}

	public int getDocLength() {
		return docLength;
	}

	public WeightingFunction getWeightingFunction() {
		return weightingFunction;
	}

	public double getWeightedValue() {
		return weightedValue;
	}

	public double getNormalizedValue() {
		return normalizedValue;
	}

	public void setNormalizedValue(double normalizedValue) {
		this.normalizedValue = normalizedValue;
	}

	public String getTerm() {
		return term;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		return builder.append("Term:").append(term).append(" Weighted Value:").append(weightedValue)
				.append(" Normalized Value:").append(normalizedValue).toString();
	}
}
