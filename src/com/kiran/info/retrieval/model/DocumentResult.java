package com.kiran.info.retrieval.model;

import java.util.Map;

public class DocumentResult {

	private Integer docId;
	private String headline;
	private String absolutePath;
	private Map<String, Vector> termVsDocVector;
	private double score;

	public DocumentResult(Integer docId, String headline, String absolutePath, Map<String, Vector> termVsDocVector) {
		super();
		this.docId = docId;
		this.headline = headline;
		this.absolutePath = absolutePath;
		this.termVsDocVector = termVsDocVector;
	}

	public Map<String, Vector> getTermVsVector() {
		return termVsDocVector;
	}

	public Integer getDocId() {
		return docId;
	}

	public String getHeadline() {
		return headline;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n\n\nDocId: ").append(docId).append("\nDoc path: ").append(absolutePath).append("\nHeadline: ")
				.append(headline).append("\nScore: ").append(score);
		builder.append("\nDocument Vectors:");
		termVsDocVector.forEach((term, value) -> {
			builder.append(value);
		});
		return builder.toString();
	}

}
