package com.kiran.info.retrieval.model;

import java.io.Serializable;

/**
 * @author KiranKumar
 *
 */
public class Token implements Serializable {

	private static final long serialVersionUID = -6790515481685187588L;
	private String word;
	private Integer frequency;

	public Token(String word, Integer frequency) {
		super();
		this.word = word;
		this.frequency = frequency;
	}

	public String getWord() {
		return word;
	}

	public Integer getFrequency() {
		return frequency;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		return builder.append("Word:").append(word).append(" Frequency:").append(frequency).toString();
	}

}
