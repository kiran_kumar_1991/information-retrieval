package com.kiran.info.retrieval.model;

public enum EncodingFormat {

	UNARY, GAMMA, DELTA

}
