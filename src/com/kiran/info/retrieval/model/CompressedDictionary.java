package com.kiran.info.retrieval.model;

import java.io.Serializable;

public class CompressedDictionary implements Serializable {

	private static final long serialVersionUID = 1822046398371062635L;
	private String terms;
	private int blockSize;
	private CompressedBlock[] blocks;

	public CompressedDictionary(String terms, int blockSize, CompressedBlock[] postings) {
		super();
		this.terms = terms;
		this.blockSize = blockSize;
		this.blocks = postings;
	}

	public String getTerms() {
		return terms;
	}

	public int getBlockSize() {
		return blockSize;
	}

	public CompressedBlock[] getCompressedBlocks() {
		return blocks;
	}

}
