---------------------------------------
# INFORMATION RETRIEVAL #
---------------------------------------

This project reads a collection of documents, tokenizes or lemmatizes the document and constructs an index for efficient retrieval of information. 

Since the document collection can be huge, index compression techniques are also implemented so that the index of large document collection can be retained efficiently in memory. 

It also supports Ranked retrieval of the results of a given query based on the requested weighting functions.

Packages / Open source code used:
----------------------------------
1) Porter Stemmer
2) Stanford Core NLP modules (version 3.5)



Steps to compile and run the code in UT Dallas CS Grads host
------------------------------------------------------------
1) Navigate to the root directory of the project.

2) Run the following command
	source /usr/local/corenlp350/classpath.sh

3) Compile the source code from the root directory using the following command. Create "bin" directory, if it doesn't exist.
    javac -cp $CLASSPATH:lib/log4j-api-2.5.jar:lib/log4j-core-2.5.jar -d bin/ src/com/kiran/info/retrieval/api/* src/com/kiran/info/retrieval/drivers/* src/com/kiran/info/retrieval/model/* src/com/kiran/info/retrieval/services/* src/com/kiran/info/retrieval/util/*

3) Execute the program from the bin directory by running the following command.
    java -cp .:$CLASSPATH:../lib/* -Dlog4j.configurationFile=../conf/log4j.xml com.kiran.info.retrieval.drivers.RankedRetrievalDriver

4) Enter the path of the document collection as the input to the program when prompted. (E.g. /people/cs/s/sanda/cs6322/Cranfield)

5) Enter the absolute path of the file containing stop words when prompted (E.g. /people/cs/s/sanda/cs6322/resourcesIR/stopwords)

6) Enter the absolute path of the file containing queries: (E.g. /people/cs/s/sanda/cs6322/hw3.queries)